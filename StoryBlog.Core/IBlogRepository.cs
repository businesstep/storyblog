﻿using System;
using System.Collections.Generic;
using System.Text;
using StoryBlog.Core.Objects;

namespace StoryBlog.Core
{
    interface IBlogRepository
    {
        IList<Post> Posts(int pageNo, int pageSize);
        int TotalPosts();
    }
}
