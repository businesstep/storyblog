﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentNHibernate.Mapping;
using StoryBlog.Core.Objects;

namespace StoryBlog.Core.Mappings
{
    class CategoryMap : ClassMap<Category>
    {
        public CategoryMap()
        {
            Id(x => x.Id);

            Map(x => x.Name)
                .Length(50)
                .Not.Nullable();

            Map(x => x.UrlSlug)
                .Length(50)
                .Not.Nullable();

            Map(x => x.Description)
                .Length(500)
                .Not.Nullable();

            HasMany(x => x.Posts)
                .Inverse()
                .Cascade.All()
                .KeyColumn("Category");
        }
    }
}
