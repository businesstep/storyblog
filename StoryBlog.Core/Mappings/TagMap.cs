﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentNHibernate.Mapping;
using StoryBlog.Core.Objects;

namespace StoryBlog.Core.Mappings
{
    class TagMap : ClassMap<Tag>
    {
        public TagMap()
        {
            Id(x => x.Id);

            Map(x => x.Name)
                .Length(50)
                .Not.Nullable();

            Map(x => x.UrlSlug)
                .Length(100)
                .Not.Nullable();

            Map(x => x.Description)
                .Length(5000);

            HasManyToMany(x => x.Posts)
                .Cascade.All().Inverse()
                .Table("PostTagMap");
        }
    }
}
